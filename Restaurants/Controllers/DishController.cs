﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Restaurants.Data;
using Restaurants.Models;

namespace Restaurants.Controllers
{
    public class DishController : Controller
    {
        private ApplicationDbContext _context;

        public DishController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Dish/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dish/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Dish dish)
        {
            try
            {
                Dish dd = _context.Dishes.FirstOrDefault(d => d.Id == dish.Id);
                if (ModelState.IsValid && dd == null)
                {
                    _context.Dishes.Add(dish);
                    _context.SaveChanges();

                    return RedirectToAction("Index", "Institution");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddToBasket(int dishId, int instituteId)
        {
            try
            {
                ApplicationUser user = _context.Users.FirstOrDefault(u => u.Email == HttpContext.User.Identity.Name);
                Basket basket = _context.Baskets
                    .FirstOrDefault(d => d.DishId == dishId && d.InstitutionId == instituteId && d.UserId == user.Id);
                if (basket == null)
                {
                    _context.Baskets.Add(new Basket()
                    {
                        Dish = _context.Dishes.FirstOrDefault(d => d.Id == dishId),
                        User = user,
                        InstitutionId = instituteId,
                        DishCount = 1
                    });
                    _context.SaveChanges();
                }
                else
                {
                    basket.DishCount++;
                }
                _context.SaveChanges();
                return RedirectToAction("Details", "Institution", new {id = instituteId});
            }
            catch
            {
                return View();
            }
        }
    }
}