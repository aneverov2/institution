﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Restaurants.Data;
using Restaurants.Models;
using Restaurants.Services;
using Restaurants.ViewModels;

namespace Restaurants.Controllers
{
    public class InstitutionController : Controller
    {
        private ApplicationDbContext _context;
        private FileUploadService _fileUploadService;
        private IHostingEnvironment _environment;

        public InstitutionController(
            ApplicationDbContext context, 
            FileUploadService fileUploadService,
            IHostingEnvironment environment)
        {
            _context = context;
            _fileUploadService = fileUploadService;
            _environment = environment;
        }

        // GET: Institution
        public ActionResult Index()
        {
            List<Institution> list = _context
                .Institutions.ToList();
            return View(list);
        }

        // GET: Institution/Details/5
        public ActionResult Details(int id)
        {
            Institution istitute = _context.Institutions.Include(i => i.Dishes)
                .FirstOrDefault(i => i.Id == id);
            ApplicationUser user = _context.Users
                .Include(u => u.Baskets)
                .FirstOrDefault(u => u.Email == HttpContext.User.Identity.Name);
            if (user != null)
            {
                List<Basket> list = _context.Baskets
                    .Include(b => b.Dish)
                    .Where(b => b.UserId == user.Id && b.InstitutionId == id)
                    .ToList();
                double summ = _context.Baskets.Include(b => b.Dish)
                    .Where(b => b.UserId == user.Id && b.InstitutionId == id).Sum(b => b.Dish.Price * b.DishCount);
                DetailsViewModel model = new DetailsViewModel()
                {
                    User = user,
                    Institution = istitute,
                    Baskets = list,
                    Summ = summ
                };
                return View(model);
            }
            else
            {
                DetailsViewModel model = new DetailsViewModel()
                {
                    Institution = istitute
                };
                return View(model);
            }
            
            
        }

        // GET: Institution/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Institution/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateInstitutionModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Institutions.Add(new Institution()
                    {
                        Name = model.Name,
                        Description = model.Description,
                        InstitutionImagePath = $"images/{model.Name}/{model.InstitutionImagePath.FileName}"
                    });
                    _context.SaveChanges();
                    string path = Path.Combine(
                        _environment.WebRootPath,
                        $"images\\{model.Name}");

                    _fileUploadService.Upload(
                        path, 
                        model.InstitutionImagePath.FileName, 
                        model.InstitutionImagePath);

                    return RedirectToAction(nameof(Index));
                }
                return View();
            }
            catch
            {
                return View();
            }
        }
    }
}