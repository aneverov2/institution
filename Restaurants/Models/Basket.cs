﻿

namespace Restaurants.Models
{
    public class Basket
    {
        public int Id { get; set; }
        public int DishCount { get; set; }
        //public int Summ { get; set; }


        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public int InstitutionId { get; set; }
        //public Institution Institution { get; set; }

        public int DishId { get; set; }
        public Dish Dish { get; set; }
    }
}
