﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Restaurants.Models
{
    public class Dish
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }

        public int InstitutionId { get; set; }
        public Institution Institution { get; set; }

        public List<Basket> Baskets { get; set; }

        public Dish()
        {
            Baskets = new List<Basket>();
        }
    }
}
