﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Restaurants.Models
{
    public class Institution
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string InstitutionImagePath { get; set; }
        public string Description { get; set; }

        public List<Dish> Dishes { get; set; }

        public Institution()
        {
            Dishes = new List<Dish>();
        }
    }
}
