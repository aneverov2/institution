﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Restaurants.Models;

namespace Restaurants.ViewModels
{
    public class CreateInstitutionModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IFormFile InstitutionImagePath { get; set; }
        public string Description { get; set; }

        public List<Dish> Dishes { get; set; }
    }
}
