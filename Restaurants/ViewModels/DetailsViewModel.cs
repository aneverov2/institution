﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Restaurants.Models;

namespace Restaurants.ViewModels
{
    public class DetailsViewModel
    {
        public ApplicationUser User { get; set; }
        public List<Basket> Baskets { get; set; }
        public Institution Institution { get; set; } 
        public double Summ { get; set; } 
    }
}
